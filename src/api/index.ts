import { Router, IRouter } from "express";
import authRoute from "./routes/auth.route";

export default (): IRouter => {
  const appRoutes: IRouter = Router();
  authRoute(appRoutes);
  return appRoutes;
};
