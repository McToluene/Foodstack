import { IRouter, Router } from "express";
import { check } from "express-validator";
import { verifyNumber } from "../controllers/auth.controller";

export default async (route: IRouter): Promise<void> => {
  const authRoutes: IRouter = Router();

  route.use("/auth", authRoutes);

  // @route   POST /api/v1/auth/verify
  // @desc    Verify a number before registration
  // @access  Public
  authRoutes.post("/verify", check("number").not().isEmpty(), verifyNumber);
};
