import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import { Logger } from "winston";
import Container from "typedi";
import { AuthService } from "../../services/AuthService";

export const verifyNumber = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
  const logger: Logger = Container.get("logger");
  logger.info("Calling auth verify number endpoint with: %o", req.body);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logger.error("Error: %o", errors);
    return res.status(422).json({ errors: errors.array() });
  }

  try {
    const authService = Container.get(AuthService);
    const { isExist } = await authService.verifyNumber(req.body.phonenumber);
    return res.status(200).json(isExist);
  } catch (error) {
    logger.error("Error: %o", error);
    return next(error);
  }
};
