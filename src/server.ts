import "reflect-metadata";

import express, { Application } from "express";
import config from "./config";
import loaders from "./loaders";
import logger from "./loaders/logger";

(async (): Promise<void> => {
  try {
    const app: Application = express();
    const PORT = config.port;

    await loaders(app);
    app.listen(PORT, () => {
      logger.info(`Server is running on http://localhost:${PORT}`);
    });
  } catch (error) {
    logger.error(error);
    process.exit(1);
  }
})();
