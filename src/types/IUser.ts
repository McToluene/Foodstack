export interface IUser {
  fullname: string;
  email: string;
  phonenumber: string;
  password: string;
}
