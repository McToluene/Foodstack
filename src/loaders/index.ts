import { Application } from "express";
import db from "./db";
import dependencyInjector from "./dependencyInjector";
import logger from "./logger";
import startUp from "./app";
import User from "../models/user.model";

export default async (appExpress: Application): Promise<void> => {
  await db();
  logger.info("Mongodb Connected!");

  const UserModel = {
    name: "UserModel",
    model: User,
  };

  await dependencyInjector({ models: [UserModel] });
  await startUp({ appInstance: appExpress });
  logger.info("Express initialized.");
};
