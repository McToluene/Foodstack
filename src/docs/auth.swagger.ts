export const getUsers = {
  tags: ["Users"],
  description: "Returns list of all users from the system",
  operationId: "getUsers",
  security: [
    {
      bearerAuth: [],
    },
  ],
  responses: {
    "200": {
      description: "A list of pets.",
      content: {
        "application/json": {
          schema: {
            type: "array",
            items: {
              fullname: {
                type: "string",
                description: "User full name",
              },
              email: {
                type: "string",
                description: "User email address",
              },
            },
          },
        },
      },
    },
  },
};
