import { getUsers } from "./auth.swagger";

export const swaggerDocument = {
  openapi: "3.0.1",
  info: {
    version: "1.0.0",
    title: "APIs Document",
    description: "Food stack authorization microservice",
    termsOfService: "Check back later",
    contact: {
      name: "Ojulari Abdulhamid",
      email: "toluenelarry@gmail.com",
      phone_number: "+2349072672303",
    },
    license: {
      name: "Apache 2.0",
      url: "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
  },
  server: [
    {
      url: "http://localhost:3000/api/v1",
      description: "Local server",
    },

    {
      url: "",
      description: "Development env",
    },
  ],
  components: {
    schemas: {},
    securitySchemas: {
      bearerAuth: {
        type: "http",
        scheme: "bearer",
        bearerFormat: "JWT",
      },
    },
  },
  tags: [
    {
      name: "Users",
    },
  ],
  paths: {
    "/users": {
      get: getUsers,
    },
  },
};
