import { Schema, model, Document } from "mongoose";
import { IUser } from "../types/IUser";

const userModel = new Schema({
  fullname: {
    type: String,
    required: [true, "Please enter user full name!"],
  },

  email: {
    type: String,
    required: [true, "Please enter user email!"],
  },

  phonenumber: {
    type: String,
    required: [true, "Please enter user phone number!"],
    unique: true,
  },
});

export default model<IUser & Document>("User", userModel);
